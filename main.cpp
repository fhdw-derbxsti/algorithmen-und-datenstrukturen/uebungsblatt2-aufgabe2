﻿#include <iostream>
#include <cstring>
#include <cctype>

using namespace std;

bool istPalindrom(char zeichenkette[], int n) {
    // Basisfall: Wenn die Zeichenkette leer oder nur ein Zeichen lang ist, ist es ein Palindrom.
    if (n <= 1) {
        return true;
    }

    // Vergleiche das erste und das letzte Zeichen, ignoriere Groß & Kleinschreibung.
    //if (tolower(zeichenkette[0]) != tolower(zeichenkette[n - 1])) {
    //    return false; // Wenn sie unterschiedlich sind, ist es kein Palindrom.
    //}

    // Vergleiche das erste und das letzte Zeichen
    if (zeichenkette[0] != zeichenkette[n - 1]) {
        return false; // Wenn sie unterschiedlich sind, ist es kein Palindrom.
    }

    // Rekursiver Schritt: Untersuche die dazwischenliegende Zeichenkette.
    return istPalindrom(zeichenkette + 1, n - 2);
}

int main() {
    char zeichenkette1[] = "reliefpfeiler";
    char zeichenkette2[] = "erikafeuertnuruntreuefakire";
    char zeichenkette3[] = "programmierenlernen";
    char zeichenkette4[] = "Reliefpfeiler";

    if (istPalindrom(zeichenkette1, strlen(zeichenkette1))) {
        cout << "Zeichenkette 1 ist ein Palindrom." << endl;
    } else {
        cout << "Zeichenkette 1 ist kein Palindrom." << endl;
    }

    if (istPalindrom(zeichenkette2, strlen(zeichenkette2))) {
        cout << "Zeichenkette 2 ist ein Palindrom." << endl;
    } else {
        cout << "Zeichenkette 2 ist kein Palindrom." << endl;
    }

    if (istPalindrom(zeichenkette3, strlen(zeichenkette3))) {
        cout << "Zeichenkette 3 ist ein Palindrom." << endl;
    } else {
        cout << "Zeichenkette 3 ist kein Palindrom." << endl;
    }
    
    if (istPalindrom(zeichenkette4, strlen(zeichenkette4))) {
        cout << "Zeichenkette 4 ist ein Palindrom." << endl;
    } else {
        cout << "Zeichenkette 4 ist kein Palindrom." << endl;
    }

    return 0;
}